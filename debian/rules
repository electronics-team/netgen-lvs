#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

CHANGELOG_DATE ?= $(shell LC_ALL=C date -u -d @$(SOURCE_DATE_EPOCH) +"%d %B %Y")

%:
	dh $@  --with autoreconf

override_dh_auto_configure:
	dh_auto_configure -- --libdir=\$${prefix}/lib

override_dh_auto_install:
	dh_auto_install
	# /usr/bin/netgen is used by another package, move binary to /usr/lib/netgen-lvs/bin/netgen:
	mkdir -p debian/netgen-lvs/usr/lib/netgen/bin
	mv debian/netgen-lvs/usr/bin/netgen debian/netgen-lvs/usr/lib/netgen/bin/netgen
	# Do not want this arbitrary doc directory under usr/lib/netgen. It is
	# installed via the debian/docs file instead.
	$(RM) -r debian/netgen-lvs/usr/lib/netgen/doc
	# #!/bin/env not allowed. Replace with #!/usr/bin/env:
	sed -i 's;^#!/bin/env;#!/usr/bin/env;' debian/netgen-lvs/usr/lib/netgen/python/*.py
	# Make scripts executable:
	chmod +x debian/netgen-lvs/usr/lib/netgen/ntk2adl.sh debian/netgen-lvs/usr/lib/netgen/spice
	# csh is considered harmful. Replace with /bin/sh
	sed -i 's;#!/bin/csh;#!/bin/sh;' debian/netgen-lvs/usr/lib/netgen/ntk2adl.sh

override_dh_clean:
	dh_clean
	$(RM) netgen/tclnetgen.so
	$(RM) debian/man/*.1

override_dh_installman:
	cd debian/man ; CHANGELOG_DATE="$(CHANGELOG_DATE)" ./genmanpages.sh
	dh_installman
